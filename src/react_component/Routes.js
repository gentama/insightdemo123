import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import HR from "./hr/Index";
import staff from "./staff/Index";
import error from "./404";
import signIn from "./signin";
import { connect } from "react-redux";
import { Button, Modal, Checkbox,Alert } from "antd";
import { isLogin } from "./auth";
import "../CSS/index.scss";
import icon from "../Public/icon.svg";
import { login } from "./auth";
import jwtDecode from "jwt-decode";
import validator from "email-validator";

class Index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      loading: false,
      email: "",
      password: "",
      remember: false,
      alert:false,
      alertMessage:""
    };
  }

componentDidMount() {
  document.title='Insight management system'
}


  showModal = () => {
    const token = localStorage.jwt_token
      ? localStorage.jwt_token
      : sessionStorage.jwt_token;
    if (token) {
      const auth = jwtDecode(token).auth;
      if (auth === "admin") {
        this.props.history.push("/hr");
      }
      if (auth === "user") {
        this.props.history.push("/staff");
      }
    } else {
      this.setState({
        visible: true,
        alert:false
      });
    }
  };

  handleInputChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  handleOk = () => {
    this.setState({ loading: true });
    const { email, password, remember } = this.state;
    if (validator.validate(email)||email==="admin"){
    login(email, password, remember)
      .then(token => {
        this.setState({ loading: false });
        const auth = jwtDecode(token).auth;
        if (auth === "admin") {
          this.props.history.push("/hr");
        }
        if (auth === "user") {
          this.props.history.push("/staff");
        }
      })
      .catch(err => {
        this.setState({ loading: false,alert:true,alertMessage:"Email or Password Incorrect!" });
        // alert("Email or Password Incorrect!");
      });}else{
        this.setState({ loading: false,alert:true,alertMessage:"Please input a valid Email address!" });
      }
  };

  handleCancel = () => {
    this.setState({ visible: false });
  };

  onCheckBoxChange=(e)=> {
    this.setState({ remember: e.target.checked });
  }

  render() {
    return (
      <div className="Index">
        <div className="IndexTop">
          <img alt="icon" src={icon}></img>
          <h2>Please choose your service:</h2>
        </div>
        <div className="service">
          <div className="Hr_service">
            <Button
              className="button"
              type="normal"
              size="large"
              onClick={this.showModal}
            >
              I'm HR manager
            </Button>
          </div>
          <div className="Staff_service">
            <Button
              className="button"
              type="normal"
              size="large"
              onClick={this.showModal}
            >
              I'm employee
            </Button>
          </div>
        </div>
        <Modal
          visible={this.state.visible}
          title="login (HR login: admin-1; stuff login:1@a.com-1)"
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          footer={[
            <Button key="back" onClick={this.handleCancel}>
              Return
            </Button>,

            <Button
              key="submit"
              type="primary"
              loading={this.state.loading}
              onClick={this.handleOk}
            >
              Submit
            </Button>
          ]}
        >
          <div className="modal_Form">
          {this.state.alert ? (
            <Alert
            style={{position:"absolute",top:0}}
              className="alertBar"
              message={this.state.alertMessage}
              type="error"
              closable
              showIcon
              afterClose={() => {
                this.setState({ alert: false });
              }}
            />
          ) : null}
            <form onSubmit={this.onFormSubmit}>
              <div className="modal_Email">
                <span>Input your Email address:</span>
                <input
                  name="email"
                  type="email"
                  value={this.state.email}
                  onChange={this.handleInputChange}
                  placeholder="Email address"
                  required
                ></input>
              </div>
              <div className="modal_Password">
                <span>Input your password:</span>
                <input
                  name="password"
                  type="password"
                  value={this.state.password}
                  onChange={this.handleInputChange}
                  placeholder="password"
                  required
                ></input>
              </div>
              <Checkbox onChange={this.onCheckBoxChange}>Remember me</Checkbox>
            </form>
          </div>
        </Modal>
        <div className="foot">
          <p>Designed by Gary Huang</p>
        </div>
      </div>
    );
  }
}

class route extends React.Component {
  
  checkAuth = () => {
    const token = localStorage.jwt_token
      ? localStorage.jwt_token
      : sessionStorage.jwt_token;
    if (token) {
      return jwtDecode(token).auth;
    }
  };

  ProtectedRoute = ({ component: ProtectedComponent, path, ...rest }) => {
    const auth = this.checkAuth();
    const match =
      (auth === "admin" && path.includes("/hr")) ||
      (auth === "user" && path.includes("/staff"));
    return (
      <Route
        {...rest}
        render={routeProps =>
          isLogin() && match ? (
            <ProtectedComponent {...routeProps} />
          ) : (
            <Redirect
              to={{
                pathname: "/signin"
                // state: { from: routeProps.location }
              }}
            />
          )
        }
      />
    );
  };

  render() {
    const ProtectedRoute = this.ProtectedRoute;
    return (
      <Switch>
        <Route exact path="/" component={Index} />
        <Route exact path="/signin" component={signIn} />
        <ProtectedRoute path="/hr" component={HR} />
        <ProtectedRoute path="/staff" component={staff} />
        <Route component={error} />
      </Switch>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLogin: state.isLogin
  };
};

export default connect(mapStateToProps)(route);
