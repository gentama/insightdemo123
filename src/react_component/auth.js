import axios from 'axios';
import jwtDecode from 'jwt-decode';

export function login(email, password, remember) {
  return axios
    .post('/auth', {
      email,
      password,
    })
    .then((auth) => {
      const { token } = auth.data;
      // sessionStorage.setItem
      if (remember) {
        localStorage.setItem('jwt_token', token);
        axios.defaults.headers.common.Authorization = `Bearer ${localStorage.jwt_token}`;
      } else {
        sessionStorage.setItem('jwt_token', token);
        axios.defaults.headers.common.Authorization = `Bearer ${sessionStorage.jwt_token}`;
      }
      return token;
    });
}

export function logout() {
  localStorage.removeItem('jwt_token');
  sessionStorage.removeItem('jwt_token');
}

export function isLogin() {
  let token = localStorage.jwt_token
    ? localStorage.jwt_token
    : sessionStorage.jwt_token;
  // you can check token expire here,decode(jwt)->get exp ->compare exp with current time
  if (token) {
    const exp = jwtDecode(token).exp * 1000;
    const now = new Date().getTime();
    if (exp > now) {
      axios.defaults.headers.common.Authorization = `Bearer ${token}`;
    } else {
      localStorage.removeItem('jwt_token');
      sessionStorage.removeItem('jwt_token');
      token = null;
    }
  }
  // else{localStorage.removeItem('jwt_token')}
  return !!token;
}
