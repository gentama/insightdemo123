import React, { Component } from 'react';
import Particles from 'react-particles-js';

class ParticlesContainer extends Component {
  render() {
    return (
      <Particles
        height="750px"
        params={{
          particles: {
            number: {
              value: 50,
            },
            size: {
              value: 3,
            },
            color: {
              value: '#000000',
            },
            shape: {
              stroke: {
                'stoke.width': 5,
                'stoke.color': '#63b5e5',
              },
              type: 'triangle',
            },
            line_linked: {
              distance: 300,
              color: '#63b5e5',
              width: 2.4,
            },
            move: {
              out_mode: 'bounce',
            },
          },
          interactivity: {
            events: {
              onhover: {
                enable: true,
                mode: 'repulse',
              },
              onclick: {
                enable: true,
                mode: 'push',
              },
            },
          },

        }}
      />
    );
  }
}

export default ParticlesContainer;
