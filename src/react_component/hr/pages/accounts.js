import React from "react";
import axios from "axios";
import jwtDecode from "jwt-decode";
import { Button, Input, Radio, Modal, Dropdown, Menu, Icon } from "antd";
import "../../../CSS/account.scss";

export default class accounts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: true,
      loading: false,
      auth: "admin",
      email: "",
      password: "",
      admin: [],
      user: [],
      department: "",
      role: "",
      name: "",
      phone: ""
    };
  }

  componentDidMount() {}

  getAuth() {
    const token = localStorage.jwt_token
      ? localStorage.jwt_token
      : sessionStorage.jwt_token;
    if (token) {
      const auth = jwtDecode(token).auth;
      return auth;
    }
  }
  showModal = () => {
    this.setState({
      visible: true
    });
  };

  handleOk = () => {
    this.setState({ loading: true });
    const { email, password, auth, name, department, role, phone } = this.state;
    const avatar="https://insightdemo-image.s3-ap-southeast-2.amazonaws.com/default_avatar.svg"
    axios
      .post(
        "/user/",
        {
          email: email,
          name: name,
          department: department,
          role: role,
          phone: phone,
          avatar:avatar,
          user: { password: password, auth: auth, New: true }
        }
      )
      .then(res => {
        alert("success");
      })
      .catch(err => {
        console.log("err");
      });
    this.setState({ loading: false, visible: false });
  };

  handleCancel = () => {
    this.setState({ visible: false });
  };

  onChange = e => {
    // console.log(e.target.value);
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onRadioChange = e => {
    // console.log( e.target.value);
    this.setState({
      auth: e.target.value
    });
  };

  handleMenuClick = e => {
    this.setState({department:e.key})
  };

  render() {
    const menu = (
      <Menu onClick={this.handleMenuClick}>
        <Menu.Item key="Web Development">Web Development</Menu.Item>
        <Menu.Item key="Marketing">Marketing</Menu.Item>
        <Menu.Item key="App Development">App Development</Menu.Item>
        <Menu.Item key="Support">Support</Menu.Item>
        <Menu.Item key="Accounting">Accounting</Menu.Item>
      </Menu>
    );
    return (
      <div>
        <div className="AddUser">
          <h3>your account state:{this.getAuth()}</h3>
          <Button type="primary" onClick={this.showModal}>
            add new user
          </Button>

          <Modal
            title="Create New User"
            visible={this.state.visible}
            onOk={this.handleOk}
            onCancel={this.handleCancel}
            footer={[
              <Button key="back" onClick={this.handleCancel}>
                Return
              </Button>,
              <Button
                key="submit"
                type="primary"
                loading={this.state.loading}
                onClick={this.handleOk}
              >
                Submit
              </Button>
            ]}
          >
            <div className="newAccountInput">
              <p>email:</p>
              <Input onChange={this.onChange} size="small" name="email" />
              <p>password:</p>
              <Input.Password
                onChange={this.onChange}
                size="small"
                name="password"
              />
              <Radio.Group
                onChange={this.onRadioChange}
                defaultValue={this.state.value}
              >
                <Radio value={"user"}>User</Radio>
                <Radio value={"admin"}>Admin</Radio>
              </Radio.Group>
              <p>name:</p>
              <Input onChange={this.onChange} size="small" name="name" />
              <div   style={{marginTop:20, marginBottom:10}}>
              <Dropdown.Button overlay={menu} icon={<Icon type="bank" /> }>
                {this.state.department||'Department'}
              </Dropdown.Button>
              </div>
              <p>role:</p>
              <Input onChange={this.onChange} size="small" name="role" />
              <p>phone:</p>
              <Input onChange={this.onChange} size="small" name="phone" />
            </div>
          </Modal>
        </div>
      </div>
    );
  }
}

// <p>department:</p>
//               <Input onChange={this.onChange} size="small" name="department" />
