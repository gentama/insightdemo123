import React from "react";
import axios from "axios";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import interactionPlugin from "@fullcalendar/interaction";
import { Progress,  Table,  Spin } from "antd";

export default class attendance extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      event: [],
      columns: [
        {
          title: "Name",
          dataIndex: "name",
          key: "name",
          align:"center",
          sorter: (a, b) => a.name.localeCompare(b.name),
        },
        {
          title: "Total Recreation Leave",
          dataIndex: "total",
          key: "total",
          align:"center",
          sorter: (a, b) => a.total-b.total,
        },
        {
          title: "Remaining Paid Leave",
          dataIndex: "remain",
          key: "remain",
          align:"center",
          sorter: (a, b) => a.remain-b.remain,
        },
        {
          title: "Percentage",
          dataIndex: "percentage",
          key: "percentage",
          render: (text, record, index) => 
          <Progress percent={((30-record.total)/30)*100} />
        }
      ],
      loading: true
    };
  }

  // [{},{
  //   name:"",
  //   total:"",
  //   percentage:""
  // }]

  convertTime = (start, end) => {
    const x = new Date(start);
    const y = new Date(end);
    return parseInt(y - x) / 1000 / 3600 / 24;
  };

  componentDidMount() {
    axios
      .get(`/profile/?isPagination=no`)
      .then(res => {
        const newEvent = [];
        // console.log(res.data);
        const newArray = res.data.filter(list => {
          return list.leave.length > 0;
        });
        newArray.forEach(e => {
          newEvent.push(...e.leave);
        });
        const event = newEvent.filter(e => e.isApproved === "Approved");
        event.forEach(e => {
          e["title"] = `${e["name"]}-${e["type"]}`;
          e["start"] = e["StartDate"];
          e["end"] = e["EndDate"];
          e["total"] = this.convertTime(e.StartDate, e.EndDate);
        });
        const total = {};
        event.forEach(e => {
          if (!total.hasOwnProperty(e.email)) {
            total[e.email] = e.total;
          } else {
            total[e.email] += e.total;
          }
        });
        const newData = event.filter(
          (thing, index, self) =>
            self.findIndex(
              t => t.place === thing.place && t.name === thing.name
            ) === index
        );
        newData.forEach(e=>{
          if (total.hasOwnProperty(e.email)){
            e.total=total[e.email]
          }
        })
        newData.forEach(e => {
          e["remain"] = 30-e["total"];
        });
        this.setState({
          event: newEvent,
          loading: false,
          data: newData
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    return (
      <Spin spinning={this.state.loading}>
      <div className="ggg" style={{marginBottom:100}}>
        <FullCalendar
          defaultView="dayGridMonth"
          plugins={[dayGridPlugin, interactionPlugin]}
          weekends={true}
          height={500}
          editable={false}
          selectable={true}
          aspectRatio={2}
          events={this.state.event}
        />
        </div>
        <div
        // style={{width:800}}
        >
          <Table
            columns={this.state.columns}
            dataSource={this.state.data}
            pagination={false}
            rowKey={record => record.uid}
          />
        </div>
      </Spin>
    );
  }
}
