import React from "react";
import { Collapse, Icon, Table, Divider, Spin } from "antd";
import axios from "axios";
import no from "../../../Public/no.svg";
import yes from "../../../Public/yes.svg";

export default class Leave extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      columns: [
        {
          title: "type",
          dataIndex: "type",
          key: "type"
        },
        {
          title: "reason",
          dataIndex: "reason",
          key: "reason"
        },
        {
          title: "StartDate",
          dataIndex: "StartDate",
          key: "StartDate"
        },
        {
          title: "EndDate",
          key: "EndDate",
          dataIndex: "EndDate"
        },
        {
          title: "status",
          key: "isApproved",
          dataIndex: "isApproved"
        },
        {
          title: "Action",
          key: "action",
          render: (text, record, index) => (
            <span>
              <img
              alt="yes"
                src={yes}
                style={{ height: 37 }}
                onClick={() => {
                  this.onApprove(text.email, text._id);
                }}
              />
              <Divider type="vertical" />
              <img
              alt="no"
                src={no}
                style={{ height: 35 }}
                onClick={() => {
                  this.onReject(text.email, text._id);
                }}
              />
            </span>
          )
        }
      ],
      loading: true
    };
  }

  adjustPending = (email, id, condition) => {
    const targetProfile = this.state.data.find(list => list._id === email);
    const targetIndex = this.state.data.findIndex(list => list._id === email);
    const newRequest = targetProfile.leave.find(e => e._id === id);
    const newRequestIndex = targetProfile.leave.findIndex(e => e._id === id);
    newRequest["isApproved"] = condition;
    targetProfile.leave[newRequestIndex] = newRequest;
    const newData = this.state.data;
    newData[targetIndex] = targetProfile;
    this.setState({ data: newData });
    axios
      .put(`/leave/${id}`, {
        isApproved: `${condition}`
      })
      .then(res => {
        alert(`Request has been ${condition}!`);
      })
      .catch(err => console.log(err));
  };

  onReject = (email, id) => {
    this.adjustPending(email, id, "Reject");
  };
  onApprove = (email, id) => {
    this.adjustPending(email, id, "Approved");
  };

  componentDidMount() {
    // const { id } = jwtDecode(localStorage.jwt_token);
    axios
      .get(`/profile/?isPagination=no`)
      .then(res => {
        const newData = res.data.filter(list => {
          return list.leave.length > 0;
        });
        this.setState({ data: newData, loading: false });
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    const { Panel } = Collapse;

    const customPanelStyle = {
      background: "#f7f7f7",
      borderRadius: 4,
      marginBottom: 24,
      border: 0,
      overflow: "hidden"
    };
    return (
      <div>
        <p style={customPanelStyle}>
          name--department--role--number of request
        </p>
        <Spin spinning={this.state.loading}>
          {this.state.data.map((list, index) => {
            return (
              <Collapse
                bordered={false}
                expandIcon={({ isActive }) => (
                  <Icon type="caret-right" rotate={isActive ? 90 : 0} />
                )}
              >
                <Panel
                  header={`${list.name}--${list.department}--${list.role}--${list.leave.length} requests`}
                  key="1"
                  style={customPanelStyle}
                >
                  <Table
                    columns={this.state.columns}
                    dataSource={list.leave}
                    pagination={false}
                    rowKey={record => record.uid}
                  />
                </Panel>
              </Collapse>
            );
          })}
          ,
        </Spin>
      </div>
    );
  }
}
