import React from 'react';
import { Button, Input } from 'antd';

export default class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = { profile: [] };
  }

  render() {
    return (
      <div style={{ width: 500, display: 'flex' }}>
        <div>
          <form onSubmit={this.props.onFormSubmit}>
            <span>Search by Name:</span>
            <Input
              size="default"
              name="ID"
              value={this.props.searchByName}
              onChange={this.props.onInputChange}
            />
          </form>
        </div>
        <div style={{ marginTop: 20, marginLeft: 10 }}>
          <Button type="primary" onClick={this.props.submit}>submit</Button>
        </div>
      </div>
    );
  }
}
