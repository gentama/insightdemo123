import React from "react";
import { withRouter, Link } from "react-router-dom";
import { Form, Icon, Input, Button, Checkbox, Alert } from "antd";
import { connect } from "react-redux";
import { login } from "./auth";
import "../CSS/signin.scss";
import jwtDecode from "jwt-decode";
import icon from "../Public/icon.svg";
import validator from "email-validator";

class NormalLoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      alert: ""
    };
  }

  handleClose = () => {
    this.setState({ visible: false });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      const { Email, password, remember } = values;
      if (validator.validate(Email) || Email === "admin") {
        login(Email, password, remember)
          .then(token => {
            const auth = jwtDecode(token).auth;
            if (auth === "admin") {
              this.props.history.push("/hr");
            }
            if (auth === "user") {
              this.props.history.push("/staff");
            }
          })
          .catch(err =>
            this.setState({
              visible: true,
              alert: "Email or password incorrect!"
            })
          );
      } else {
        this.setState({
          visible: true,
          alert: "Please input a valid Email address!"
        });
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="alert">
        {this.state.visible ? (
          <Alert
            className="alertBar"
            message={this.state.alert}
            type="error"
            closable
            showIcon
            afterClose={this.handleClose}
          />
        ) : null}
        <div className="signIn">
          <div className="Icon">
            <img alt="icon" src={icon}></img>
            <span>Insight</span>
          </div>
          <Form onSubmit={this.handleSubmit} className="login-form">
            <Form.Item>
              {getFieldDecorator("Email", {
                rules: [{ required: true, message: "Please input your Email!" }]
              })(
                <Input
                  style={{width:300}}
                  prefix={
                    <Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />
                  }
                  placeholder="Email"
                />
              )}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator("password", {
                rules: [
                  { required: true, message: "Please input your Password!" }
                ]
              })(
                <Input
                  prefix={
                    <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                  }
                  type="password"
                  placeholder="Password"
                />
              )}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator("remember", {
                valuePropName: "checked",
                initialValue: false
              })(<Checkbox>Remember me</Checkbox>)}
              <Button
                type="primary"
                htmlType="submit"
                className="login-form-button"
              >
                Log in
              </Button>
              <Link to="/">Return</Link>
            </Form.Item>
          </Form>
        </div>
      </div>
    );
  }
}
const WrappedNormalLoginForm = Form.create({ name: "normal_login" })(
  NormalLoginForm
);

const mapStateToProps = state => {
  return {
    isLogin: state.isLogin
  };
};

export default connect(mapStateToProps)(withRouter(WrappedNormalLoginForm));
