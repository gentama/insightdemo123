import React from 'react';
import SideNav from './SideNav';
import Routes from './route';

export default class Staff extends React.Component {


  render() {
    return (
      <div>
        <SideNav />
        <Routes />
      </div>
    );
  }
}
