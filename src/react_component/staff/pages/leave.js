import React from "react";
import axios from "axios";
import { DatePicker, Input, Button, Select, Table, Spin } from "antd";
import jwtDecode from "jwt-decode";
import Delete from "../../../Public/delete.svg";

export default class leave extends React.Component {
  state = {
    date: [],
    data: [],
    columns: [
      {
        title: "type",
        dataIndex: "type",
        key: "type"
      },
      {
        title: "reason",
        dataIndex: "reason",
        key: "reason"
      },
      {
        title: "StartDate",
        dataIndex: "StartDate",
        key: "StartDate"
      },
      {
        title: "EndDate",
        key: "EndDate",
        dataIndex: "EndDate"
      },
      {
        title: "status",
        key: "isApproved",
        dataIndex: "isApproved"
      },
      {
        title: "Action",
        key: "action",
        render: (text, record, index) => (
          <span>
            <img
            alt="delete"
              src={Delete}
              style={{ height: 35 ,cursor:"pointer"}}
              onClick={() => {
                this.onDelete(text._id);
              }}
            />
          </span>
        )
      }
    ],
    type: "",
    reason: "",
    visible: false,
    loading: true,
    name:""
  };

  componentDidMount() {
    const token = localStorage.jwt_token
    ? localStorage.jwt_token
    : sessionStorage.jwt_token;
    const { id } = jwtDecode(token);
    axios
      .get(`/profile/${id}`)
      .then(res => {
        // const depList = [];
        this.setState({ data: res.data.leave,loading:false,name:res.data.name });
        // res.data.leave.map(list => {
        //   depList.push(list.leave);
        //   return depList;
        // });
        // console.log(depList)
      })
      .catch(err => {
        console.log(err);
      });
  }

  onDelete = id => {
    const newList = this.state.data.filter(x => {
      return x._id !== id;
    });
    this.setState({ data: newList });
    axios
    .delete(`/leave/${id}`)
    .then(alert('delete success!'))
    .catch(err=>{console.log(err)})
  }

  onTypeChange = value => {
    this.setState({ type: value }, () => this.onReasonTypeCheck());
  };

  onReasonTypeCheck = () => {
    if (this.state.type === "Other") {
      this.setState({ visible: true });
    }
    if (this.state.type === "Annual") {
      this.setState({ visible: false });
      this.setState({ reason: "Annual leave" });
    }
  };

  onReasonChange = e => {
    this.setState({ reason: e.target.value });
  };

  onDateChange = (value, dateString) => {
    this.setState({ date: dateString });
  };

  onFormSubmit = e => {
    e.preventDefault();
  };

  submit = () => {
    const token = localStorage.jwt_token
    ? localStorage.jwt_token
    : sessionStorage.jwt_token;
    const { id } = jwtDecode(token);
    const { date, type, reason,name } = this.state;
    const index = Math.floor(Math.random() * 1000);
    const newList = this.state.data
    newList.push({
      _id:index,
      email: id,
      name: name,
      type: type,
      reason: reason,
      StartDate: date[0],
      EndDate: date[1],
      isApproved: 'pending'
    });
    this.setState({data:newList});
    axios
      .post(`/leave`, {
        id: index,
        email: id,
        type: type,
        name: name,
        reason: reason,
        StartDate: date[0],
        EndDate: date[1],
        isApproved: 'pending'
      })
      .then(res => {
        alert("Request has been uploaded!");
      })
      .catch(err => console.log(err));
    
  };

  render() {
    const { RangePicker } = DatePicker;
    const { Option } = Select;
    const { TextArea } = Input;
    return (
      <div>
        <form
          onSubmit={this.onFormSubmit}
          style={{ marginTop: 50, marginLeft: 50 }}
        >
          <div>
            <h3>Choose your type of request:</h3>
            <Select style={{ width: 200 }} onChange={this.onTypeChange}>
              <Option value="Annual">Annual Leave</Option>
              <Option value="Other">Others</Option>
            </Select>
          </div>
          {this.state.visible ? (
            <div>
              <h3>Your reason of leaving:</h3>
              <TextArea
                row={6}
                style={{ width: 600, height: 200 }}
                onChange={this.onReasonChange}
              ></TextArea>
            </div>
          ) : null}
          <div style={{ marginBottom: 10 }}>
            <h3>Your leaving period:</h3>
            <RangePicker
              showTime={{ format: "HH:mm" }}
              format="YYYY-MM-DD"
              placeholder={["Start Time", "End Time"]}
              onChange={this.onDateChange}
              onOk={this.onOk}
            />
          </div>
          <div>
            <span>I confirm the leaving request above is correct</span>
            <Button
              onClick={this.submit}
              type="primary"
              style={{ marginLeft: 10 }}
            >
              submit
            </Button>
          </div>
        </form>
        <Spin spinning={this.state.loading}>
          <Table
            columns={this.state.columns}
            dataSource={this.state.data}
            pagination={false}
            rowKey={record => record.uid}
          />
        </Spin>
      </div>
    );
  }
}
