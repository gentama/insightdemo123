import { Form, Spin, Input, Button, Modal, Avatar } from "antd";
import React from "react";
import axios from "axios";
import jwtDecode from "jwt-decode";
import bcrypt from "bcryptjs";
import { connect } from "react-redux";
import { avatarChange } from "../../../redux/action";

class AdvancedSearchForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      avatarloading:false,
      visible: false,
      name: "",
      phone: "",
      email: "",
      department: "",
      role: "",
      password: "",
      loading: true,
      oldPassword: "",
      newPassword: "",
      confirmPassword: "",
      image: "",
      avatar: ""
    };
  }

  showModal = () => {
    this.setState({
      visible: true
    });
  };
  submit = () => {
    this.setState({avatarloading:true})
    const token = localStorage.jwt_token
      ? localStorage.jwt_token
      : sessionStorage.jwt_token;
    const { id } = jwtDecode(token);
    const { name, phone, department, role, email } = this.state;

    const data = {
      name: name,
      phone: phone,
      department: department,
      role: role,
      email: email
    };
    const mainData = new FormData();
    Object.keys(data).forEach(key => mainData.append(key, data[key]));
    if (this.file.files && this.file.files[0]) {
      mainData.append("file", this.file.files[0]);
    }

    axios
      .put(`/profile/${id}`, mainData)
      .then(res => {
        if(this.file.files && this.file.files[0]){
          const reader = new FileReader();
          reader.readAsDataURL(this.file.files[0]);
          reader.onload = event => {
            this.setState({avatar:event.target.result},()=>{
              this.props.dispatch(avatarChange({ avatar: event.target.result }))
            });
            ;
          };
        }
        alert("Profile has been changed!");
        this.setState({avatarloading:false})
      })
      .catch(err => console.log(err));
  };

  handleImage = () => {
    if (this.file.files && this.file.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(this.file.files[0]);
      reader.onload = event => {
        this.setState({ avatar: event.target.result });
      };
    }
  };

  handleOk = e => {
    const token = localStorage.jwt_token
      ? localStorage.jwt_token
      : sessionStorage.jwt_token;
    const { id } = jwtDecode(token);
    const { oldPassword, newPassword, confirmPassword } = this.state;
    axios.get(`/profile/${id}`).then(res => {
      const { password } = res.data.user;
      bcrypt.compare(oldPassword, password).then(res => {
        if (!res) {
          alert("Old Password incorrect");
        } else {
          if (newPassword !== confirmPassword) {
            alert("newPassword have to be matched!");
          } else {
            axios
              .put(`/user/${id}`, {
                user: { password: newPassword }
              })
              .then(res => {
                alert("password has been changed!");
                localStorage.removeItem("jwt_token");
                sessionStorage.removeItem("jwt_token");
                this.props.history.push("/");
              })
              .catch(err => console.log(err));
          }
        }
      });
    });
    this.setState({
      visible: false
    });
  };

  handleCancel = e => {
    this.setState({
      visible: false
    });
  };

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  componentDidMount() {
    const token = localStorage.jwt_token
      ? localStorage.jwt_token
      : sessionStorage.jwt_token;
    const id = jwtDecode(token).id;
    axios
      .get(`/profile/${id}`)
      .then(res => {
        const { name, phone, _id, department, role, avatar } = res.data;
        this.setState({
          name: name,
          phone: phone,
          email: _id,
          department: department,
          role: role,
          avatar: avatar,
          oldPassword: "",
          newPassword: "",
          confirmPassword: "",
          loading: false
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    return (
      <Spin spinning={this.state.loading}>
        <Form className="ant-advanced-search-form" onSubmit={this.handleSearch}>
          <div style={{ display: "flex", width: 1300, flexWrap: "wrap" }}>
            <div>
              <p style={{ marginBottom: 5, marginTop: 10 }}>Name:</p>
              <Input
                disabled={true}
                onChange={this.onChange}
                size="default"
                name="name"
                style={{ width: 500, marginRight: 50, marginLeft: 50 }}
                value={this.state.name}
              />
            </div>
            <div>
              <p style={{ marginBottom: 5, marginTop: 10 }}>Email:</p>
              <Input
                disabled={true}
                onChange={this.onChange}
                size="default"
                name="email"
                style={{ width: 500, marginRight: 50, marginLeft: 50 }}
                value={this.state.email}
              />
            </div>
            <div>
              <p style={{ marginBottom: 5, marginTop: 10 }}>Department:</p>
              <Input
                disabled={true}
                onChange={this.onChange}
                size="default"
                name="department"
                style={{ width: 500, marginRight: 50, marginLeft: 50 }}
                value={this.state.department}
              />
            </div>
            <div>
              <p style={{ marginBottom: 5, marginTop: 10 }}>Role:</p>
              <Input
                disabled={true}
                onChange={this.onChange}
                size="default"
                name="role"
                style={{ width: 500, marginRight: 50, marginLeft: 50 }}
                value={this.state.role}
              />
            </div>
            <div>
              <p style={{ marginBottom: 5, marginTop: 10 }}>Phone:</p>
              <Input
                onChange={this.onChange}
                size="default"
                name="phone"
                style={{ width: 500, marginRight: 50, marginLeft: 50 }}
                value={this.state.phone}
              />
            </div>
            <div>
              <Button
                type="primary"
                onClick={this.submit}
                style={{ marginTop: 36, marginLeft: 50 }}
              >
                submit
              </Button>
              <Button
                type="primary"
                style={{ marginTop: 36, marginLeft: 50 }}
                onClick={this.showModal}
              >
                Change Password
              </Button>
            </div>
          </div>
          <div
            style={{
              marginTop: 30,
              display: "flex",
              alignItems: "center",
              marginLeft: 30
            }}
          >
            <div>
            <p style={{marginBottom:30}}>Avatar:</p>
              <input
                type="file"
                ref={e => (this.file = e)}
                onChange={this.handleImage}
              />
            </div>
            <div>
            <Spin spinning={this.state.avatarloading}>
              <Avatar
                src={this.state.avatar}
                shape="square"
                size="large"
                style={{ width: 200, height: 200 }}
              ></Avatar>
              </Spin>
            </div>
          </div>
        </Form>
        <Modal
          title="Change Password"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <p>Old Password:</p>
          <Input
            size="default"
            require={true}
            onChange={this.onChange}
            name="oldPassword"
          />
          <p>New Password:</p>
          <Input
            size="default"
            require={true}
            onChange={this.onChange}
            name="newPassword"
          />
          <p>Confirm New Password:</p>
          <Input
            size="default"
            require={true}
            onChange={this.onChange}
            name="confirmPassword"
          />
        </Modal>
      </Spin>
    );
  }
}

const mapStateToProps = state => {
  return {
    avatar: state.avatar
  };
};

const WrappedNormalLoginForm = Form.create({ name: "advanced_search" })(
  AdvancedSearchForm
);
export default connect(mapStateToProps)(WrappedNormalLoginForm);
