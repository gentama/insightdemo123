const initialState = {
  avatar: "123"
};

export default function isLogin(state = initialState, action) {
  switch (action.type) {
    case "AVATAR":
      // console.log(action.payload.avatar);
      return Object.assign({}, state, {
        avatar: action.payload.avatar
      });
    // case 'LOGOUT':
    //   return { ...state, isLogin: false };
    default:
      return state;
  }
}
